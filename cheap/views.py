from django.shortcuts import render
import collections
from django.http import HttpResponse
import requests
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from django.core.cache import cache

def index(request):
    list_routes = [{'name': 'Алматы - Астана', 'fly_from': 'ALA', 'fly_to': 'TSE'}, {'name': 'Астана - Алматы', 'fly_from': 'TSE', 'fly_to': 'ALA'}, {'name': 'Алматы - Москва', 'fly_from': 'ALA', 'fly_to': 'MOW'}, {'name': 'Москва - Алматы', 'fly_from': 'MOW', 'fly_to': 'ALA'} , {'name': 'Алматы - Шымкент', 'fly_from': 'ALA', 'fly_to': 'CIT'}, {'name': 'Шымкент-Алматы', 'fly_from': 'CIT', 'fly_to': 'ALA'}, {'name': 'Астана - Москва', 'fly_from': 'TSE', 'fly_to': 'MOW'}, {'name': 'Москва - Астана', 'fly_from': 'MOW', 'fly_to': 'TSE'} , {'name': 'Астана - Санкт-Петербург', 'fly_from': 'TSE', 'fly_to': 'LED'}, {'name': 'Санкт-Петербург - Астана', 'fly_from': 'LED', 'fly_to': 'TSE'}]
    context = { 'list_routes': list_routes}
    return render(request, 'cheap/index.html', context)

def route(request):
    fly_from = request.GET.get('fly_from')
    fly_to = request.GET.get('fly_to')
    context = { 'tickets': collections.OrderedDict(sorted(cache.get(fly_from+fly_to).items())) }
    return render(request, 'cheap/spec.html', context)

def isTicketValid(cur):
    check_flight = requests.get('https://booking-api.skypicker.com/api/v0.1/check_flights?v=2&booking_token=' + cur['booking_token'] +'&bnum=1&pnum=1&affily=picky_{market}&currency=KZT&adults=1&children=0&infants=0').json()
    return check_flight['flights_invalid'] != True

def refresh_info():
    print('started refreshing')
    cities = ['ALATSE', 'TSEALA', 'ALAMOW', 'MOWALA', 'ALACIT', 'CITALA', 'TSEMOW', 'MOWTSE', 'TSELED', 'LEDTSE']
    today = date.today()
    d1 = today.strftime("%d/%m/%Y")
    d2 = (today + relativedelta(months=1)).strftime("%d/%m/%Y")
    for city in cities:
        print('city is ' + city)
        response = requests.get('https://api.skypicker.com/flights?fly_from=' + city[:3] + '&fly_to=' + city[-3:] + '&date_from='+d1+'&date_to='+d2+'&partner=picky&curr=KZT').json()
        allFlights = response['data']
        month_tickets = {}
        for flight in allFlights:
            cdate = datetime.fromtimestamp(flight['dTimeUTC']).date()
            price = flight['price']
            current_object = {
                 'price': price,
                 'booking_token':flight['booking_token']
            }
            if cdate in month_tickets:
                if month_tickets[cdate]['price'] > price:
                    check_flight = requests.get('https://booking-api.skypicker.com/api/v0.1/check_flights?v=2&booking_token=' + current_object['booking_token'] +'&bnum=1&pnum=1&affily=picky_{market}&currency=KZT&adults=1&children=0&infants=0').json()
                    if(check_flight['flights_invalid'] != True):
                        month_tickets[cdate] = current_object
            else:
                check_flight = requests.get('https://booking-api.skypicker.com/api/v0.1/check_flights?v=2&booking_token=' + current_object['booking_token'] +'&bnum=1&pnum=1&affily=picky_{market}&currency=KZT&adults=1&children=0&infants=0').json()
                if(check_flight['flights_invalid'] != True):
                    month_tickets[cdate] = current_object
        cache.set(city, month_tickets, 86400)
